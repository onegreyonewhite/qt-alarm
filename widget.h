#ifndef WIDGET_H
#define WIDGET_H

#include <QtGui>
#include <QDesktopWidget>
#include <QSound>
#include "trayicon.h"

namespace Ui {
class Window;
}

class Window : public QWidget
{
    Q_OBJECT

public:
    explicit Window(QWidget *parent = 0);
    ~Window();

protected:
    void closeEvent(QCloseEvent * event);
    void moveToCenter();

public slots:
    void updateDateTime();

private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void start();
    void stop();

private:
    Ui::Window *ui;
    QSound *alarmSound;
    TrayIcon *tray;
    QTimer *timer;
};

#endif // WIDGET_H

#include "widget.h"
#include "ui_widget.h"

/* Конструктор окна с настройками */
Window::Window(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Window)
{
    ui->setupUi(this);
    this->moveToCenter();

    /* Создаём проигрыватель звука будильника (повтор 10 раз) */
    alarmSound = new QSound(":/smokealarm.wav");
    alarmSound->setLoops(10);

    /* Прячем кнопку остановки и задаём время в поле */
    ui->stopButton->hide();
    ui->alarmTimeEdit->setDateTime(QDateTime::currentDateTime());

    /* Создаём объект иконки в трее и выводим */
    tray = new TrayIcon(this);
    tray->show();

    /* Подключаем методы к кнопкам и иконке трея */
    connect(tray, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
    connect(ui->startButton, SIGNAL(clicked()), this, SLOT(start()));
    connect(ui->stopButton, SIGNAL(clicked()), this, SLOT(stop()));

    /* Создаём таймер */
    timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(updateDateTime()));
}

/* Деструктор класса окна настроек */
Window::~Window()
{
    delete ui;
}

/*
 * Действия при закрытии окна.
 * При закрытии окна выходить из приложения.
*/
void Window::closeEvent(QCloseEvent *event)
{
    if(timer->isActive()){
        this->stop();
    }
    tray->hide();
    event->accept();
}

/*
 * Действия при нажатии на иконку в трее:
 * - окно открыто - спрятать
 * - окно закрыто - показать
*/
void Window::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason){
    case TrayIcon::Trigger:
        /* Обработка только событий нажатия на иконку */
        if(!this->isVisible()){
            this->show();
            this->moveToCenter();
        } else {
            this->hide();
        }
        break;
    default:
        break;
    }
}

/* Действия при нажатии кнопки "Завести" */
void Window::start()
{
    /* Запуск таймера */
    timer->start(5000);

    /* Выводим сообщение в трее */
    tray->print("Будильник взведён!");

    /* Прячем окно будильника */
    this->hide();

    /* Изменяем видимость кнопок */
    ui->startButton->hide();
    ui->stopButton->show();

    /* Блокируем изменения в поле ввода времени */
    ui->alarmTimeEdit->setReadOnly(true);

    qDebug() << "Таймер запущен.";

}

/* Действия при нажатии кнопки "Остановить будильник!" */
void Window::stop()
{
    /* Выводим сообщение в трее */
    tray->print("Будильник остановлен!");

    /* Обновляем время в поле ввода до текущего */
    ui->alarmTimeEdit->setDateTime(QDateTime::currentDateTime());

    /* Изменяем видимость кнопок */
    ui->startButton->show();
    ui->stopButton->hide();

    /* Разрешаем редактирование поле ввода времени */
    ui->alarmTimeEdit->setReadOnly(false);

    /* Остановка таймера */
    timer->stop();

    /* Остановить сигнал */
    alarmSound->stop();

    /* Возвращаем размер */
    this->showNormal();
    this->resize(430, 179);

    qDebug() << "Таймер остановлен.";
}

/* Обработчик таймера */
void Window::updateDateTime()
{
    const QDateTime currentDT = QDateTime::currentDateTime();
    QDateTime targetDT = ui->alarmTimeEdit->dateTime();

    if(currentDT >= targetDT){
        /* Делаем интервал вывода сообщений в 2 сек. */
        timer->setInterval(2000);

        /* Выводим сообщение в трее */
        tray->print("Сработал будильник!!!");

        /* Открываем окно если не открыто и запускаем сигнал */
        if(!this->isVisible()){
            this->showMaximized();
            alarmSound->play();
            ui->stopButton->setFocus();
            ui->stopButton->setDefault(true);
            ui->stopButton->setAutoDefault(true);
        }
    }
    if(this->ui->stopButton->isVisible()){
        qDebug() << "Текущее время:" << currentDT.toString()
                 << " Время срабатывания" << targetDT.toString();
    }
}

/* Метод отображения окна по центру экрана */
void Window::moveToCenter()
{
    this->move(qApp->desktop()->availableGeometry(this).center()-rect().center());
}

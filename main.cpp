#include "widget.h"
#include <QApplication>
#include <QtGui>

/* Точка ввхода приложения */
int main(int argc, char *argv[])
{
    qDebug() << "Запуск приложения";

    /* Создаём объект приложения, назначаем имя и иконку */
    QApplication app(argc, argv);
    app.setApplicationName("Будильник");

    /* Создаём и отображаем главное окно */
    Window w;
    w.show();

    /* запускаем обработчик приложения */
    return app.exec();
}

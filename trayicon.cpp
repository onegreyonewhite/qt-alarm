#include "trayicon.h"

/* Конструктор иконки в трее */
TrayIcon::TrayIcon(QWidget *parent) :
    QSystemTrayIcon(parent)
{
    /* Задаём сообщение-подсказку для иконки */
    setToolTip("Будильник" "\n"
               "Демонстрационная программа-будильник.");

    /* Устанавливаем иконку как у окна-родителя */
    setIcon(parent->windowIcon());
}

/*
 * Метод для работы с сообщениями из трея.
 * Получает массив символов. Иконка информационная.
*/
void TrayIcon::print(const char *message)
{
    this->showMessage("Будильник", QString(message), QSystemTrayIcon::Information, 2000);
}

#ifndef TRAYICON_H
#define TRAYICON_H
#include <QWidget>
#include <QSystemTrayIcon>

class TrayIcon : public QSystemTrayIcon
{
    Q_OBJECT

public:
    explicit TrayIcon(QWidget *parent = 0);
    void print(const char *message);
};

#endif // TRAYICON_H
